class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :location
      t.string :user_id
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
