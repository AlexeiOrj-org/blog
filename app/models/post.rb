class Post < ActiveRecord::Base
  belongs_to :user
  validates :title, :desciption, presence: true

  mount_uploader :image, ImageUploader
end
