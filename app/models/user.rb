class User < ActiveRecord::Base
  attr_accessor :password
  has_one  :profile, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :accounts, dependent: :destroy
  has_many :favourite_posts, dependent: :destroy
  before_save :encrypt_password
  validates_presence_of :email
  # validates_uniqueness_of :email
  validates_confirmation_of :password
  validates_presence_of :password, on: :create

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    end
  end

  def encrypt_password
    return if password.blank?
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end
end
