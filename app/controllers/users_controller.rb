class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :change_permission]
  before_action :require_user, except: [:new, :create]

  def index
    @users = User.order(:email)
  end

  def show; end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @profile = @user.create_profile
      redirect_to edit_user_profile_path(@user.id, @profile.id)
    else
      render :new
    end
  end

  def edit; end

  def update
    if @user.update(user_params)
      flash[:success] = 'Profile updated'
      redirect_to @user
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
  end

  def change_permission
    @user.update(admin: !@user.admin)
    redirect_to :back
  end

  private

  def require_user
    redirect_to root_path unless current_user
  end

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
