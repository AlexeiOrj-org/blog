class ProfilesController < ApplicationController
  before_action :current_profile, only: [:edit, :update]
  def edit
    @user = User.find(params[:user_id])
  end

  def update
   
    if @profile.update(profile_params)
      flash[:success] = 'Profile updated'
      redirect_to root_path
    else
      render :edit
    end
  end

  private
  def current_profile
     @profile = Profile.find(params[:id])
  end

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :location, :avatar)
  end
end