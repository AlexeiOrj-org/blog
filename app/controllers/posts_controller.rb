class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy, :favourite]
  before_action :require_user

  def index
    @posts = Post.all
    render :index
  end

  def show
    @post.increment!(:page_views)
  end

  def new
    @post = Post.new
  end

  def edit; end

  def create
    @post = current_user.posts.build(post_params)
    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Пост успешно создан.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Пост успешно обновлен.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Пост успешно удалено.' }
    end
  end

  def favourite
    current_user.favourite_posts.manage(@post)
  end

  private

  def require_user
    redirect_to root_path unless current_user
  end

  def set_post
    @post = Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :desciption, :image)
  end
end
