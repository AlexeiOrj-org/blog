#
module PostsHelper
  def favourite_post(post)
    if current_user.favourite_posts.map(&:post).include?(post)
      'text-success'
    else
      'text-muted'
    end
  end
end
