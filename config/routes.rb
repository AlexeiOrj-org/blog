Rails.application.routes.draw do
  root 'sessions#new'
  get 'auth/vkontakte/callback', to: 'omniauth#create'

  resources :users do
    resources :profiles, only: [:edit, :update]
    get :change_permission, on: :member
  end

  resources :posts do
    get :favourite, on: :member
  end

  resources :sessions, only: [:new, :create] do
    delete :destroy, on: :collection
    get :login_with, on: :member
  end
end
